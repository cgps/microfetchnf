## Command to run fetch
Prepare an `accessions.txt` file with a list of run accessions e.g SRR or ERR....... (1 accession per line) and run the command

```
nextflow run main.nf \
--input accessions.txt \
-profile singularity -resume
```
