#!/usr/bin/env python3
import json
import argparse
import urllib.request, json

# Parse input arguments
parser = argparse.ArgumentParser(description='Gathers url given an accession number.')
parser.add_argument('acc_num', metavar='accession_number', type=str,
                    help='accession number of the genome to be downloaded')
parser.add_argument('--out_file', type=str, required=False,
                    help='if given the url(s) will be saved to this file')
args = parser.parse_args()



ACC_NUM = args.acc_num #"SRR9600327"
BASE_URL = "https://www.ebi.ac.uk/ena/portal/api/filereport?result=read_run&fields=fastq_ftp&format=JSON&accession="

# Retrieve information from ebi website
with urllib.request.urlopen(f"{BASE_URL}{ACC_NUM}") as url:
    try:
        data = json.loads(url.read().decode())
    except:
        print(f"No data was associated with {ACC_NUM}")
        data = [{"fastq_ftp" : f"{ACC_NUM}: No data"}]

# Print output
if args.out_file:
    OUT_NAME = args.out_file
    out = open(OUT_NAME,"w")
    for item in data:
        out.write('\n'.join(item['fastq_ftp'].split(';')) + "\n")
        out.close()
else:
    for item in data:
        print(item["fastq_ftp"].strip())