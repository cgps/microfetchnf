nextflow.enable.dsl=2
// Don't overwrite global params.modules, create a copy instead and use that within the main script.
def modules = params.modules.clone()

// Local: Modules
include { GET_FTP_LINKS }             from './modules/get_ftp_links' addParams( modules['get_ftp_links'] )
include { DOWNLOAD_FASTQS }           from './modules/download_fastqs'
include { REPORT_IGNORED_ACCESSIONS } from './modules/report_ignored_accessions'

// Read in ids from --input file
Channel
    .from(file(params.input, checkIfExists: true))
    .splitCsv(header:false, sep:'', strip:true)
    .map { it[0] }
    .unique()
    .set { ch_accessions }

workflow {
    GET_FTP_LINKS(ch_accessions)

    GET_FTP_LINKS.out.num_links_and_paths
    .branch {
        num_links_and_path ->
        ACCESSIONS_TO_IGNORE: num_links_and_path[0].toInteger() < 2
        ACCESSIONS_TO_DOWNLOAD: num_links_and_path[0].toInteger() == 2
    }
    .set { num_links_and_paths_branch }

    num_links_and_paths_branch.ACCESSIONS_TO_IGNORE
        .map { it[1] }
        .set { ignored_accessions }
    
    REPORT_IGNORED_ACCESSIONS(ignored_accessions.collect())

    num_links_and_paths_branch.ACCESSIONS_TO_DOWNLOAD
        .map { [it[1], it[2]] }
        .set { accessions_to_download }
    
    DOWNLOAD_FASTQS(accessions_to_download)

}
