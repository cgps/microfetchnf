process DOWNLOAD_FASTQS {
    tag "$accession"
    publishDir "${params.outdir}/fastqs", mode: 'move'

    input:
    tuple val(accession), path(links)

    output:
    path("*.fastq.gz")

    script:
    """
    sleep ${params.sleep_prior_to_download}
    INDEX=0
    for LINK in \$(cat $links)
    do
        INDEX=\$((INDEX+1))
        curl \\
            --retry 5 --continue-at - --retry-delay 10 --max-time 600 \\
            -L \${LINK} \\
            -o ${accession}_\${INDEX}.fastq.gz
    done
    """
}