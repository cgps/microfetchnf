process GET_FTP_LINKS {
    tag "$accession"

    if (workflow.containerEngine == 'singularity' ) {
        container 'https://depot.galaxyproject.org/singularity/python:3.9--1'
    } else {
        container 'quay.io/biocontainers/python:3.9--1'
    }

    input:
    val accession

    output:
    tuple env(NUM_READ_FILES), val(accession), path("links.txt"), emit: num_links_and_paths

    script:
    """
    get_ftp_links.py $accession --out links.txt
    NUM_READ_FILES=\$(cat links.txt | wc -l)
    """
}